import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app.routing';
import { NavbarModule } from './shared/navbar/navbar.module';
import { FooterModule } from './shared/footer/footer.module';
import { SidebarModule } from './sidebar/sidebar.module';

import { AppComponent } from './app.component';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { ParkingAreaComponent } from './parking-area/parking-area.component';
import { UsersComponent } from './users/users.component';
import { CamerasComponent } from './cameras/cameras.component';
import { ComplaintsComponent } from './complaints/complaints.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { TestComponent } from './test/test.component';
import { ToastrModule } from 'ngx-toastr';
import { ResizableDraggableComponent } from './resize/resize.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    NavbarModule,
    ReactiveFormsModule,
    FooterModule,
    SidebarModule,
    AppRoutingModule,
    ToastrModule.forRoot()
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    ProfileComponent,
    ParkingAreaComponent,
    UsersComponent,
    CamerasComponent,
    ComplaintsComponent,
    MyProfileComponent,
    TestComponent,
    ResizableDraggableComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
