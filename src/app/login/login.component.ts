import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { apiEndpoint, environment } from 'environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userData: FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });
  forgotPassword: any = false;
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
  }
  loginUser() {
    console.log(this.userData.value);
    this.http.post(apiEndpoint + 'user/token/', this.userData.value).subscribe((res: any) => {
      localStorage.setItem('user', JSON.stringify(res));
      localStorage.setItem('token', res.token.toString());
      this.router.navigate(['/my-profile']);
    });
  }
}
