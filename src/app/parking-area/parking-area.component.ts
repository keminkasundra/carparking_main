import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ResizableDraggableComponent } from 'app/resize/resize.component';
import { apiEndpoint } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-parking-area',
  templateUrl: './parking-area.component.html',
  styleUrls: ['./parking-area.component.css']
})
export class ParkingAreaComponent implements OnInit {
  parkingAreaDetails: any = {
    address: '',
    image: '',
    imageTemp: ''
  }
  
  points: any = [];
  pointsTemp: any = [];
  imageSrc: string = '';
  parkings: any = [];
  top=100;
  left=100;
  height=100;
  width=100;
  markingadding = false
  @ViewChild(ResizableDraggableComponent) resizer: ResizableDraggableComponent;
  constructor(private http: HttpClient, private toastr: ToastrService) { 
    this.getCameras();
  }

  ngOnInit(): void {
  }
  saveMarkings() {
    console.log(this.resizer);
    // console.log(this.resizer.left, this.resizer.top, this.resizer.width, this.resizer.height);
    // this.addPoint(this.resizer.left, this.resizer.top);
    // this.addPoint(this.resizer.left+this.resizer.width, this.resizer.top);
    // this.addPoint(this.resizer.left+this.resizer.width, this.resizer.top+this.resizer.height);
    // this.addPoint(this.resizer.left, this.resizer.top+this.resizer.height);
    this.markingadding = false;
  }
  addPoint(e) {
    let c: any = document.getElementById("myCanvas");
    let rect = c.getBoundingClientRect();
    let x = Number( e.clientX - rect.left );
    let y = Number(e.clientY - rect.top);
    console.log("Coordinate x: " + x, 
                "Coordinate y: " + y);
                let ctx = c.getContext("2d");
                ctx.strokeStyle = '#000000';
                ctx.fillRect(x-2,y-2,4,4)
                this.pointsTemp.push([x,y]);
                if(this.pointsTemp.length > 3) {
                  this.points.push(this.pointsTemp);
                  this.pointsTemp = [];
                this.drawPoints();
    }
  }
  
  drawPoints() {
    let c: any = document.getElementById("myCanvas");
    let ctx = c.getContext("2d");
    for (let box of this.points) {
        ctx.beginPath();
        ctx.moveTo.apply(ctx, box[0]); // es5 friendly
        ctx.lineTo(...box[1]); // es6+
        ctx.lineTo(...box[2]);
        ctx.lineTo(...box[3]);
        ctx.lineTo(...box[0]);
        ctx.fillStyle = '#000000';
        ctx.stroke();
      }
  
  // let c: any = document.getElementById("myCanvas");
  //     let ctx = c.getContext("2d");
  //     let img = document.getElementById("imagePlacer");
  //     ctx.drawImage(img, 10, 10);

}
  onFileChange(event) {
    const reader = new FileReader();
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.parkingAreaDetails.image = event.target.files.item(0);
      reader.readAsDataURL(file);
    
      reader.onload = () => {
        this.imageSrc = reader.result as string;
        let c: any = document.getElementById("myCanvas");
        let ctx = c.getContext("2d");
        let img = document.getElementById("imagePlacer");
        console.log(img)
       setTimeout(() => {
        ctx.drawImage(img, 0, 0, 500, 300);
       }, 2000);
      };
   
    }
  }



  addCamera() {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
    // if(this.parkingAreaDetails.id) {
    //   const formData = new FormData();
    //   formData.append('address', this.parkingAreaDetails.address);
    //   formData.append('image', this.parkingAreaDetails.image, this.parkingAreaDetails.image.name);
    //   this.http.put(apiEndpoint + 'car/add_parking_map/'+this.parkingAreaDetails.id, this.parkingAreaDetails, {headers: headers}).subscribe(res => {
    //     this.getCameras();
    //     this.parkingAreaDetails = {
    //       address: '',
    //       image: '',
    //       imageTemp: ''
    //     }
    //   });
    //   return;
    // }
    
    const formData = new FormData();
    formData.append('address', this.parkingAreaDetails.address);
    formData.append('image', this.parkingAreaDetails.image);
    if(!this.parkingAreaDetails.address) {
      this.toastr.success('Please add address first!');
      return;
    }
    if(!this.parkingAreaDetails.image) {
      this.toastr.success('Please Select first!');
      return;
    }
    if(!(this.points && this.points.length > 0)) {
      this.toastr.success('Please add markings to proceed!');
      return;
    }
    this.http.post(apiEndpoint + 'car/add_parking_map', formData, {headers: headers}).subscribe((res: any) => {
      const payload = {
        id: res.data.id,
        placement: JSON.stringify({
          points: this.points,
          resolution: [500, 300]
        })
      }
      
      this.http.put(apiEndpoint+'car/update_parking_map', payload, {headers: headers}).subscribe((res: any) => {
       
      });
      this.getCameras();
      this.parkingAreaDetails = {
        address: '',
        image: '',
        imageTemp: ''
      }
    });
  }
  delete() {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
    if(this.parkingAreaDetails.id) {
      this.http.delete(apiEndpoint + 'camera/delete_camera/'+this.parkingAreaDetails.id, {headers: headers}).subscribe(res => {
        this.getCameras();
        this.parkingAreaDetails = {
          address: '',
          image: '',
          imageTemp: ''
        }
      });
      return;
    }
  }
  getCameras() {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
    
    this.http.get(apiEndpoint + 'car/parking_map', {headers: headers}).subscribe((res: any) => {
      this.parkings = res.data;
    });
  }



}
