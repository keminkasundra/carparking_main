import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { apiEndpoint } from 'environments/environment';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  userData = {
    "id": '',
    "name": "",
    "email": "",
    "password": "",
    "emirates_id": "",
    "mobile_no": "",
    "address": "",
    "status": "",
    "type": "",
    "access": "",
    "parking_areas": "",
    "allAccess": {
      user: false,
      test: false,
      parking: false,
      camera: false,
      complaint: false,
    },
    "allParkings": {

    }
  }
  parkings: [];
  users: [];
  constructor(private http: HttpClient) { }
  setDefaultData() {
    this.userData = {
      "id": '',
      "name": "",
      "email": "",
      "password": "",
      "emirates_id": "",
      "mobile_no": "",
      "address": "",
      "status": "",
      "type": "",
      "access": "",
      "parking_areas": "",
      "allAccess": {
        user: false,
        test: false,
        parking: false,
        camera: false,
        complaint: false,
      },
      "allParkings": {
  
      }
    }
  }
  addUser() {
    for (let parking in this.userData.allParkings) {
      if (this.userData.allParkings[parking]) {
        this.userData.parking_areas =this.userData.parking_areas + ',' + parking;
      }
    };
    for (let parking in this.userData.allAccess) {
      if (this.userData.allAccess[parking]) {
        this.userData.access =this.userData.access + ',' + parking;
      }
    };
    this.userData.parking_areas.replace(',', '');
    this.userData.access.replace(',', '');
    if (this.userData && this.userData.id) {
      let headers = new HttpHeaders();
      headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
      
      this.http.put(apiEndpoint + 'user/update_delete_user/' + this.userData.id, this.userData, {headers: headers}).subscribe((res: any) => {
       this.setDefaultData();
      });
    } else {
      let headers = new HttpHeaders();
      headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
      
      this.http.post(apiEndpoint + 'user/add_user/', this.userData, {headers: headers}).subscribe((res: any) => {
        this.setDefaultData();
      });
    }
    
  }
  ngOnInit(): void {
    this.getParkings();
    this.getUsers();
  }
getParkings() {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
    
    this.http.get(apiEndpoint + 'car/parking_map', {headers: headers}).subscribe((res: any) => {
      this.parkings = res.data;
      this.parkings.map((parking: any) => {
        this.userData.allParkings[parking.address] = false;
      });
    });
  }
  getUsers() {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
    
    this.http.get(apiEndpoint + 'user/get_users/', {headers: headers}).subscribe((res: any) => {
      this.users = res.data;
     
    });
  }
}