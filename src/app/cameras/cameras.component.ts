import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { apiEndpoint } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';
declare var google;
@Component({
  selector: 'app-cameras',
  templateUrl: './cameras.component.html',
  styleUrls: ['./cameras.component.css']
})
export class CamerasComponent implements OnInit {
  fileToUpload: File | null = null;
  cameraDetails: any ={
    camera_url: '',
    co_ordinates: "",
    parking_area: 0,
    id: 0
}
map: any;
parkings: any = [];
ngAfterViewInit() {
    const myLatLng = { lat: 25.2048, lng: 55.2708 };
  
    this.map = new google.maps.Map(
      document.getElementById("googleMap") as HTMLElement,
      {
        zoom: 10,
        center: myLatLng,
      }
    );
  
}
getParkings() {
  let headers = new HttpHeaders();
  headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
  
  this.http.get(apiEndpoint + 'car/parking_map', {headers: headers}).subscribe((res: any) => {
    this.parkings = res.data;
  });
}
cameras: any = [];
  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.getCameras();
    this.getParkings();
    setTimeout(() => {
      const myLatLng = { lat: 25.2048, lng: 55.2708 };

      var marker = new google.maps.Marker({
        position: myLatLng,
        title:"Hello World!"
      });
      marker.setMap(this.map);
      const myLatLng2 = { lat: 25.21, lng: 55.30 };

      var marker = new google.maps.Marker({
        position: myLatLng2,
        title:"Hello World!"
      });
      marker.setMap(this.map);
    }, 2000);
   }

  ngOnInit(): void {
  }
  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
}
addCamera() {
  let headers = new HttpHeaders();
  headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
  
  if(!this.cameraDetails.camera_url) {
    this.toastr.success('Please add camera url!');
    return;
  }
  if(!this.cameraDetails.co_ordinates) {
    this.toastr.success('Please add camera coordinates!');
    return;
  }
  if(!this.cameraDetails.co_ordinates) {
    this.toastr.success('Please add camera coordinates!');
    return;
  }
  if(this.cameraDetails.id) {
    this.http.put(apiEndpoint + 'camera/update_camera/'+this.cameraDetails.id, this.cameraDetails, {headers: headers}).subscribe(res => {
      this.getCameras();
      this.submit(this.cameraDetails.id);
    });
    return;
  }
  
  if(!this.fileToUpload) {
    this.toastr.success('Please add video!');
    return;
  }
  this.http.post(apiEndpoint + 'camera/add_camera', this.cameraDetails, {headers: headers}).subscribe(res => {
    this.getCameras(true);
   
  });
}
delete() {
  let headers = new HttpHeaders();
  headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
  if(this.cameraDetails.id) {
    this.http.delete(apiEndpoint + 'camera/delete_camera/'+this.cameraDetails.id, {headers: headers}).subscribe(res => {
      this.getCameras();
      this.cameraDetails ={
        camera_url: '',
        co_ordinates: "",
        parking_area: 0,
        id: 0
    }
    });
    return;
  }
}
getCameras(flag = false) {
  let headers = new HttpHeaders();
  headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
  
  this.http.get(apiEndpoint + 'camera/get_cameras', {headers: headers}).subscribe((res: any) => {
    this.cameras = res.data;
    if (this.cameras && this.cameras.length) {
for (let cam of this.cameras) {
  const coordinats = cam.co_ordinates.split(',');
if (coordinats.length == 2) {
  const myLatLng = { lat: Number(coordinats[0].replaceAll(' ', '')), lng: Number(coordinats[1].replaceAll(' ', '')) };

  var marker = new google.maps.Marker({
    position: myLatLng,
    title:"Hello World!"
  });
  marker.setMap(this.map);
}
  
}
    }
    if (flag) {
      this.submit(this.cameras.at(-1).id);
      this.cameraDetails ={
        camera_url: '',
        co_ordinates: "",
        parking_area: 0,
        id: 0
    }
    }
  });
}
startTest() {
  this.parkings.map((par: any) => {
    if(par.id == 18) {
      const payload = new FormData();
      payload.append('camera_url', 'http://18.118.226.189/media/Camera_' + '1' + '.mp4');
      payload.append('placement', par.placement);
      this.http.post('http://52.14.22.200:8080/start_process', payload).subscribe((res: any) => {
        
      });
    }
  })
}
submit(id) {
  if(this.fileToUpload) {
    const formData = new FormData();
    let fileArray = this.fileToUpload.name.split('.');
    
  fileArray[0] = 'Camera_'+id;
    const fileName = fileArray.join('.');
    formData.append('video_file', this.fileToUpload, fileName);
    this.http.post(apiEndpoint + 'upload_video', formData).subscribe(res => {
      console.log(res);
    })
  }
}
}
