import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { apiEndpoint } from 'environments/environment';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})
export class TablesComponent implements OnInit {
    public tableData1: TableData;
    public tableData2: TableData;
    mapDdetails: any;
  constructor(private http: HttpClient) { }

  ngOnInit() {
      this.tableData1 = {
          headerRow: ["Id", 'Address', 'Live stream'],
          dataRows: []
      };
      
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
        this.http.get(apiEndpoint+'car/parking_map', {headers: headers}).subscribe((res: any) => {
            this.tableData1.dataRows = res.data;
        });
    }
    updateDetectStatus() {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
    const payload = {
      id: this.mapDdetails.id,
      is_detecting: !this.mapDdetails.is_detecting
    }
        this.http.post(apiEndpoint+'car/update_parking_map_detection', payload, {headers: headers}).subscribe((res: any) => {
          this.mapDdetails.is_detecting =  !this.mapDdetails.is_detecting;
        });
    }
    openMap(data) {
      this.mapDdetails = data;
      setTimeout(_ => {
        let canvas: any = document.body.querySelector("#myCanvas");
    
    let ctx = canvas.getContext('2d');

    const drawLine = (X1, Y1, X2, Y2) => {
      console.log(X1, X2, Y1,Y2);
      ctx.save();
      ctx.beginPath();
      ctx.translate(X1, Y1)
      ctx.moveTo(X1, Y1);
      ctx.lineTo(X2, Y2);
      ctx.stroke();
      ctx.restore();
    }
    console.log(this.mapDdetails);
    const placement = JSON.parse(this.mapDdetails.placement);
    console.log(placement);
   if(placement && placement.length) {
    for (let box2 of placement) {
      const box = box2;
      for (let a of box) {
        a[0] = a[0]/2;
        a[1] = a[1]/2;
      }
      ctx.beginPath();
      ctx.moveTo.apply(ctx, box[0]); // es5 friendly
      ctx.lineTo(...box[1]); // es6+
      ctx.lineTo(...box[2]);
      ctx.lineTo(...box[3]);
      ctx.lineTo(...box[0]);
      ctx.lineTo(...box[2]);
      ctx.lineTo(...box[3]);
      ctx.lineTo(...box[1]);


      ctx.strokeStyle = '#ff0000';
      ctx.stroke();
    }
    } else if(placement && placement.points){
      for (let box of placement.points) {
        for (let a of box) {
          a[0] = a[0]*1.6;
          a[1] = a[1]*1.4;
        }
        ctx.beginPath();
        ctx.moveTo.apply(ctx, box[0]); // es5 friendly
        ctx.lineTo(...box[1]); // es6+
        ctx.lineTo(...box[2]);
        ctx.lineTo(...box[3]);
        ctx.lineTo(...box[0]);
        
      ctx.lineTo(...box[2]);
      ctx.lineTo(...box[3]);
      ctx.lineTo(...box[1]);
        ctx.fillStyle = '#000000';
        ctx.stroke();
      }
    }  else {
      ctx.beginPath();
      ctx.fillStyle = '#00ff00';
      ctx.font = "bold 16px Arial";
      ctx.fillText( 'No parkings available', 5, 10);
      ctx.stroke();
    }

      }, 500);
    }
}
