import { Component, OnInit } from '@angular/core';
import { LocationStrategy, PlatformLocation, Location } from '@angular/common';
import { LegendItem, ChartType } from '../lbd/lbd-chart/lbd-chart.component';
import * as Chartist from 'chartist';
import { apiEndpoint } from 'environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    public emailChartType: ChartType;
    public emailChartData: any;
    public emailChartLegendItems: LegendItem[];

    public hoursChartType: ChartType;
    public hoursChartData: any;
    public hoursChartOptions: any;
    public hoursChartResponsive: any[];
    public hoursChartLegendItems: LegendItem[];

    public activityChartType: ChartType;
    public activityChartData: any;
    public isAdmin: any = false;

    public activityChartOptions: any;
    public activityChartResponsive: any[];
    public activityChartLegendItems: LegendItem[];
    mapData: any = {
      address: '',
      url: '',
      image: null
    };
    mapDetais: FormGroup = new FormGroup({
      address: new FormControl(''),
      url: new FormControl(''),
      image: new FormControl(null)
    });
    imageSrc: any = '';
    points: any = [];
    pointsTemp: any = [];
  constructor(private http: HttpClient, private router: Router) { }
  onFileChange(event) {
    const reader = new FileReader();
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.mapData.image = event.target.files[0];
    this.mapDetais.get('image').setValue(event.target.files[0])
      reader.readAsDataURL(file);
    
      reader.onload = () => {
        this.imageSrc = reader.result as string;
        let c: any = document.getElementById("myCanvas");
        let ctx = c.getContext("2d");
        let img = document.getElementById("imagePlacer");
        console.log(img)
       setTimeout(() => {
        ctx.drawImage(img, 0, 0, 500, 300);
       }, 2000);
      };
   
    }
  }
  addPoint(e) {
    let c: any = document.getElementById("myCanvas");
    let rect = c.getBoundingClientRect();
    let x = e.clientX - rect.left;
    let y = e.clientY - rect.top;
    console.log("Coordinate x: " + x, 
                "Coordinate y: " + y);
                let ctx = c.getContext("2d");
                ctx.strokeStyle = '#000000';
                ctx.fillRect(x-2,y-2,4,4)
                this.pointsTemp.push([x,y]);
                if(this.pointsTemp.length > 3) {
                  this.points.push(this.pointsTemp);
                  this.pointsTemp = [];
                this.drawPoints();
    }
  }
    drawPoints() {
      let c: any = document.getElementById("myCanvas");
      let ctx = c.getContext("2d");
      for (let box of this.points) {
          ctx.beginPath();
          ctx.moveTo.apply(ctx, box[0]); // es5 friendly
          ctx.lineTo(...box[1]); // es6+
          ctx.lineTo(...box[2]);
          ctx.lineTo(...box[3]);
          ctx.lineTo(...box[0]);
          ctx.fillStyle = '#000000';
          ctx.stroke();
        }
    
    // let c: any = document.getElementById("myCanvas");
    //     let ctx = c.getContext("2d");
    //     let img = document.getElementById("imagePlacer");
    //     ctx.drawImage(img, 10, 10);

  }
  addMap() {
    console.log(this.mapDetais.value);
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
    const formData = new FormData();
    formData.append('address', this.mapDetais.value.address);
    formData.append('image', this.mapDetais.value.image);
    formData.append('url', this.mapDetais.value.url);
    this.http.get(apiEndpoint+'car/parking_map', {headers: headers}).subscribe((res: any) => {
      const list = res.data.map(map => map.id);
      this.http.post(apiEndpoint + 'car/add_parking_map', formData, {headers: headers}).subscribe((res: any) => {
        this.http.get(apiEndpoint+'car/parking_map', {headers: headers}).subscribe((res: any) => {
          const list2 = res.data.map(map => map.id);
          list2.map(mapD => {
            if(list.indexOf(mapD) == -1) {
              const payload = {
                id: mapD,
                placement: JSON.stringify({
                  points: this.points,
                  resolution: [500, 300]
                })
              }
              
              this.http.put(apiEndpoint+'car/update_parking_map', payload, {headers: headers}).subscribe((res: any) => {
                this.router.navigate(['/table'])
              });
            }
          })
         });    
      });
     });
  }
  ngOnInit() {
    const userStr = localStorage.getItem('user');
    const user = userStr ? JSON.parse(userStr) : {};
    if(!user){
      this.router.navigate(['/login']);
      return;
    }
    if(user && user.user_id == 13) {
      this.isAdmin = true;
    }
      this.emailChartType = ChartType.Pie;
      this.emailChartData = {
        labels: ['62%', '32%', '6%'],
        series: [62, 32, 6]
      };
      this.emailChartLegendItems = [
        { title: 'Open', imageClass: 'fa fa-circle text-info' },
        { title: 'Bounce', imageClass: 'fa fa-circle text-danger' },
        { title: 'Unsubscribe', imageClass: 'fa fa-circle text-warning' }
      ];

      this.hoursChartType = ChartType.Line;
      this.hoursChartData = {
        labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
        series: [
          [287, 385, 490, 492, 554, 586, 698, 695, 752, 788, 846, 944],
          [67, 152, 143, 240, 287, 335, 435, 437, 539, 542, 544, 647],
          [23, 113, 67, 108, 190, 239, 307, 308, 439, 410, 410, 509]
        ]
      };
      this.hoursChartOptions = {
        low: 0,
        high: 800,
        showArea: true,
        height: '245px',
        axisX: {
          showGrid: false,
        },
        lineSmooth: Chartist.Interpolation.simple({
          divisor: 3
        }),
        showLine: false,
        showPoint: false,
      };
      this.hoursChartResponsive = [
        ['screen and (max-width: 640px)', {
          axisX: {
            labelInterpolationFnc: function (value) {
              return value[0];
            }
          }
        }]
      ];
      this.hoursChartLegendItems = [
        { title: 'Open', imageClass: 'fa fa-circle text-info' },
        { title: 'Click', imageClass: 'fa fa-circle text-danger' },
        { title: 'Click Second Time', imageClass: 'fa fa-circle text-warning' }
      ];

      this.activityChartType = ChartType.Bar;
      this.activityChartData = {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        series: [
          [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895],
          [412, 243, 280, 580, 453, 353, 300, 364, 368, 410, 636, 695]
        ]
      };
      this.activityChartOptions = {
        seriesBarDistance: 10,
        axisX: {
          showGrid: false
        },
        height: '245px'
      };
      this.activityChartResponsive = [
        ['screen and (max-width: 640px)', {
          seriesBarDistance: 5,
          axisX: {
            labelInterpolationFnc: function (value) {
              return value[0];
            }
          }
        }]
      ];
      this.activityChartLegendItems = [
        { title: 'Tesla Model S', imageClass: 'fa fa-circle text-info' },
        { title: 'BMW 5 Series', imageClass: 'fa fa-circle text-danger' }
      ];


    }

}
