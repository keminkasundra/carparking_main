import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { apiEndpoint } from 'environments/environment';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  mapDdetails: any;
  tableData1: any;
  secondVideo: any;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.tableData1 = {
      headerRow: ["Id", 'Address', 'Live stream'],
      dataRows: []
  };
  
let headers = new HttpHeaders();
headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
    this.http.get(apiEndpoint+'car/parking_map', {headers: headers}).subscribe((res: any) => {
        this.tableData1.dataRows = res.data;
    });
}
openMap(data) {
  this.mapDdetails = data;
  setTimeout(_ => {
    this.startTracking();
  }, 500);
}
startTracking() {
  let canvas: any = document.body.querySelector("#myCanvas");
  let ctx = canvas.getContext('2d');
  this.secondVideo = this.mapDdetails.id % 2;
  let headers = new HttpHeaders();
  headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
      this.http.get('http://52.14.22.200:8080/', {headers: headers}).subscribe((res: any) => {
        setTimeout(_ => {
          this.startTracking();
        }, 100)
        let video: any = document.body.querySelector("#myVideo");
        console.log(video);
     setTimeout(() => {
          video.pause();
    video.currentTime = res.data[this.mapDdetails.id].frame_number/30;
     }, 50);
ctx.clearRect(0, 0, canvas.width, canvas.height);

const response =  res.data[this.mapDdetails.id].values;
for (let box2 of response) {
  console.log(box2);
  if(box2.coordinates) {
    let box = [];
    for (let a in box2.coordinates) {box.push(box2.coordinates[a])};
    console.log(box);
    for (let a of box) {
      a[0] = a[0]/2;
      a[1] = a[1]/2;
    }
    ctx.beginPath();
    ctx.moveTo.apply(ctx, box[0]); // es5 friendly
    ctx.lineTo(...box[1]); // es6+
    ctx.lineTo(...box[2]);
    ctx.lineTo(...box[3]);
    ctx.lineTo(...box[0]);
    ctx.lineTo(...box[2]);
    ctx.lineTo(...box[3]);
    ctx.lineTo(...box[1]);
    ctx.strokeStyle = !box2.occupiedStatus ? '#00ff00' : '#ff0000';
    ctx.fillStyle =  !box2.occupiedStatus ? '#00ff00' : '#ff0000';
    ctx.font = "bold 16px Arial";
    ctx.fillText( !box2.occupiedStatus ? 'A' : 'O',box[3][0] + 5, box[3][1]+ 20);
    ctx.stroke();
  }
};
}, _ => {
  setTimeout(_ => {
    this.startTracking();
  }, 100);
});
}
}
