import { Routes } from '@angular/router';

import { HomeComponent } from '../../home/home.component';
import { UserComponent } from '../../user/user.component';
import { TablesComponent } from '../../tables/tables.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { LoginComponent } from 'app/login/login.component';
import { ProfileComponent } from 'app/profile/profile.component';
import { ParkingAreaComponent } from 'app/parking-area/parking-area.component';
import { UsersComponent } from 'app/users/users.component';
import { CamerasComponent } from 'app/cameras/cameras.component';
import { ComplaintsComponent } from 'app/complaints/complaints.component';
import { MyProfileComponent } from 'app/my-profile/my-profile.component';
import { TestComponent } from 'app/test/test.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard',      component: HomeComponent },
    { path: 'user',           component: UserComponent },
    { path: 'table',          component: TablesComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'upgrade',        component: UpgradeComponent },
    { path: 'profile',        component: ProfileComponent },
    { path: 'parking-area',        component: ParkingAreaComponent },
    { path: 'users',        component: UsersComponent },
    { path: 'cameras',        component: CamerasComponent },
    { path: 'complaints',        component: ComplaintsComponent },
    { path: 'my-profile',        component: MyProfileComponent },
    { path: 'test',        component: TestComponent },
   
];
