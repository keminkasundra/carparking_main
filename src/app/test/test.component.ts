import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { apiEndpoint } from 'environments/environment';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  cameras: any = [];
  cameraSelected: any = 0;
  parkings: any = [];
  date: any = 123456;
  entries: any = [];
  occupied = {};
  counter: number = 0;
  constructor(private http: HttpClient, private ds: DomSanitizer) { 
    this.getParkings();
    this.getCameras();
  }
getUrl() {
  // return this.ds.bypassSecurityTrustUrl();
}
  ngOnInit(): void {
  }
  
getParkings() {
  let headers = new HttpHeaders();
  headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
  
  this.http.get(apiEndpoint + 'car/parking_map', {headers: headers}).subscribe((res: any) => {
    this.parkings = res.data;
  });
}
  startTest() {
    
    const video: any = document.getElementById('video');
    const url = 'http://18.118.226.189/media/Camera_' + this.cameraSelected + '.mp4';
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';
    xhr.onload = function(e) {
    if (this.status == 200) {
      var myBlob = this.response;
      video.src = window.URL.createObjectURL(myBlob);
      // myBlob is now the blob that the object URL pointed to.
    }
    };
    xhr.send()
    this.cameras.map((cam: any) => {
      if (this.cameraSelected == cam.id) {
    //     this.cameraSelected = 0;
    //     setTimeout(() => {
    // this.cameraSelected = temp;
    //     }, 2000);
        this.parkings.map((par: any) => {
          if(par.id == cam.parking_area) {
            const payload = new FormData();
            this.date = new Date();
            payload.append('camera_url', 'http://18.118.226.189/media/Camera_' + cam.id + '.mp4');
            payload.append('placement', par.placement);
            this.http.post('http://52.14.22.200:8080/start_process', payload).subscribe((res: any) => {
              // this.getDetectings();             
            });
            this.getDetectings(video);  
          }
        })
      }
    })
  }
  getDetectings(video) {
    this.http.get('http://52.14.22.200:8080/').subscribe((res: any) => {
      if(video && res && res.data && res.data[0] && res.data[0].frame_number) 
      {
        
        this.counter++;
        if(this.counter > 10) {
          video.currentTime = res.data[0].frame_number / 24;
          this.counter = 0;
        } 
        
     this.drawPoints(res);
      }
      setTimeout(() => {
      this.getDetectings(video);  
      }, 500);           
    });
  }
  drawPoints(data) {
    if(data && data.data && data.data[0] && data.data[0].values){
      
      let c: any = document.getElementById("myCanvas");
      let ctx = c.getContext("2d");
      ctx.clearRect(0, 0, 500, 400);
      let points = data.data[0].values;
      for (let box2 in points) {
let box = points[box2].coordinates;
console.log( points[box2].occupiedStatus, box2, points);
if(this.entries && this.entries.length < 3) {
  this.entries.push(
    {
      time: new Date(),
      status: false,
      plat: '',
      index: box2
    }
  )
}
if(!points[box2].occupiedStatus && this.occupied[box2]) {
  this.entries.push(
    {
      time: new Date(),
      status: false,  
      plat: box.Number_plate,
      index: box2
    }
  )
}
if(points[box2].occupiedStatus && !this.occupied[box2]) {
  this.entries.push(
    {
      time: new Date(),
      status: true,
      plat: box.Number_plate,
      index: box2
    }
  )
}
console.log(this.occupied);
this.occupied[box2] = points[box2].occupiedStatus;

ctx.fillStyle = box.occupiedStatus ? '#ff0000' : '#00ff00';
          ctx.beginPath();
          ctx.moveTo.apply(ctx, box.x1); // es5 friendly
          ctx.lineTo(...box.x2); // es6+
          ctx.lineTo(...box.y1);
          ctx.lineTo(...box.y2);
          ctx.lineTo(...box.x1);
          ctx.fillStyle = points[box2].occupiedStatus ? '#ff0000' : '#00ff00';
          ctx.stroke();
        }
    
    }
    
  // let c: any = document.getElementById("myCanvas");
  //     let ctx = c.getContext("2d");
  //     let img = document.getElementById("imagePlacer");
  //     ctx.drawImage(img, 10, 10);

}
  getCameras() {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
    
    this.http.get(apiEndpoint + 'camera/get_cameras', {headers: headers}).subscribe((res: any) => {
      this.cameras = res.data;
    });
  }
}
