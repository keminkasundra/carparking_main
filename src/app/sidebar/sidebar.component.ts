import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { apiEndpoint } from 'environments/environment';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    // { path: '/dashboard', title: 'Add Map',  icon: 'pe-7s-graph', class: '' },
    // { path: '/table', title: 'Map List',  icon:'pe-7s-note2', class: '' },
    // { path: '/user', title: 'Map View',  icon:'pe-7s-user', class: '' },
    
    { path: '/my-profile', title: 'My Profile',  icon:'pe-7s-user', class: '' },
    { path: '/cameras', title: 'Cameras',  icon:'pe-7s-user', class: '' },
    { path: '/parking-area', title: 'Parking Area',  icon:'pe-7s-user', class: '' },
    { path: '/users', title: 'Users',  icon:'pe-7s-user', class: '' },
    { path: '/complaints', title: 'Complaints',  icon:'pe-7s-user', class: '' },
    { path: '/test', title: 'Test',  icon:'pe-7s-user', class: '' },
    ];

    export const ROUTESUSER: RouteInfo[] = [
      { path: '/dashboard', title: 'Parkings',  icon: 'pe-7s-graph', class: '' },
      { path: '/profile', title: 'Profile',  icon:'pe-7s-note2', class: '' },
      ];
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  menuItemsUser: any[];
  isAdmin: any = false;
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.menuItemsUser = ROUTESUSER.filter(menuItem => menuItem);

    const userStr = localStorage.getItem('user');
    const user = userStr ? JSON.parse(userStr) : {};
    if(user && user.user_id == 13) {
      this.isAdmin = true;
    }
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  }
  clearAndLogout() {
    
  let headers = new HttpHeaders();
  headers = headers.append('Authorization', 'Token ' + localStorage.getItem('token'));
    
  this.http.delete(apiEndpoint + 'delete_data', {headers: headers}).subscribe((res: any) => {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.router.navigate(['/login'])
  });
  }
}
